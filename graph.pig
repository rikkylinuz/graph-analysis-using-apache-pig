G = load '$G' using PigStorage(',') as (x: long, y: long);
G_firstgrouped = group G by x;
G_firstreduce = foreach G_firstgrouped generate group, COUNT(G);
G_proj_grouped = group G_firstreduce by $1;
G_secondreduce = foreach G_proj_grouped generate group, COUNT(G_firstreduce);
store G_secondreduce into '$O' using PigStorage(' ');

