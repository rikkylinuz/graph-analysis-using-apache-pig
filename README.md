<h3>Graph Analysis using Apache pig</h3>

Implemented a simple graph algorithm to group the nodes with their number of neighbors. For each group, count the number of nodes present in that group using Pig.
